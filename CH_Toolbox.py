__author__ = 'gstocking'

import pandas as pd
import os, glob
import re
import urllib
from urlparse import urlparse
import datetime as dt
import collections
import tldextract
import requests

def extract_hash_tags(s):
    return set(part[1:] for part in s.split() if part.startswith('#'))

def extract_hosts(s):
    match = re.search("(?P<url>https?://[^\s]+)", s)
    result = ''
    if match is not None:
        try:
            '''
            resp = urllib.urlopen(match.group('url'))
            if resp.getcode() == 200:
                tld = tldextract.extract(urlparse(resp.url).hostname)
                result = '.'.join([tld.domain, tld.suffix])
            '''
            resp = requests.get(match.group('url'))
            url = resp.url
            tld = tldextract.extract(url)
            result =  '.'.join([tld.domain, tld.suffix])
        except:
            pass
    return result

def extract_original_tweeter(tweet, pattern):
    m_auth = re.search(pattern, tweet)
    if m_auth:
        return m_auth.group(1)
    else:
        return ''

def normalize_hashtag(s):
    p = re.compile(r'[\W_]+', re.UNICODE)
    return p.sub('', s.lower())

class CH_Toolbox():

    # General Functions
    def load_files(self, dir_name):
        df = pd.DataFrame()
        for file in os.listdir(dir_name):
            if file.endswith(".xls"):
                print file
                xl = pd.ExcelFile(os.path.join(dir_name, file))
                df2 = xl.parse(xl.sheet_names[0])
                df = pd.concat([df,df2])
        # Replace Date (GMT) with Date
        df['Date'] = df['Date (GMT)']

        return df

    # Basic descriptive statistics functions
    # - Hashtag Count
    # - Mentions Count (edgelist for Gephi) (not included for now)
    # - Hosts Count
    # - Retweet Count (Assumes a DF of just retweets for now)


    def get_retweet_list(self, df):
        return df['Contents'].value_counts()

    def get_hashtag_list(self, df):
        x = df['Contents'].apply(lambda x : extract_hash_tags(x))
        hashtags=dict()
        for s in x:
            for ht in s:
                ht = normalize_hashtag(ht)
                if hashtags.has_key(ht):
                    hashtags[ht] = hashtags[ht] + 1
                else:
                    hashtags[ht] = 1
        return [(k, v) for v, k in sorted([(v,k) for k,v in hashtags.items()], reverse=True)]

    def get_hosts_list(self, df):
        df['Domain'] = df['Contents'].apply(lambda x: extract_hosts(x))
        return df
        '''
        hosts = dict()
        for i in range(0, len(df)):
            s = df.iloc[i]['Contents']
            match= re.search("(?P<url>https?://[^\s]+)", s)
            if match is not None:
                resp = urllib.urlopen(match.group("url"))
                if resp.getcode() == 200:
                    tld = tldextract.extract(urlparse(resp.url).hostname)
                    if hosts.has_key(tld):
                        hosts[tld] += 1
                    else:
                        hosts[tld] = 1
        sorted_hosts = [(k, v) for v, k in sorted([(v,k) for k,v in hosts.items()], reverse=True)]
        return pd.DataFrame(sorted_hosts)

        '''

    # Assumes df has a Domain field; ie. that it has been run through get_hosts_list first
    def get_hosts_count(self, df):
        return df['Domain'].value_counts()

    # Author related functions. Each gives data aggregated per author
    # - Overall Count
    # - Daily Count
    # - Hashtags Count
    # - Mentions Count
    # - Hosts Count
    # ----
    # The following currently require a dataset of starting tweets and a
    #  separate dataset of retweets
    # - Retweets Count
    # - Retweets Timeline (requires max days)
    # - Author / Category Retweet Stats (# Tweets, # Retweets)
    def get_author_counts(self, df):
        return df['Author'].value_counts()

    def get_author_daily_counts(self, df):
        resamp = df.set_index('Date').groupby('Author').resample('D', how='count')
        l = []
        for ind, frame in resamp.iterrows():
            d = {}
            d.update({'Author': ind[0], 'Date':dt.datetime.strftime(ind[1], '%Y-%m-%d'), 'Count':frame[0]})
            l.append(d)
        return pd.DataFrame(l, columns=['Author', 'Date', 'Count'])

    def get_author_category_daily_counts(self, df):
        resamp = df.set_index('Date').groupby(['Author', 'Category']).resample('D', how='count')
        l = []
        for ind, frame in resamp.iterrows():
            d = {}
            d.update({'Author': ind[0], 'Category': ind[1], 'Date':dt.datetime.strftime(ind[2], '%Y-%m-%d'), 'Count':frame[0]})
            l.append(d)
        return pd.DataFrame(l, columns=['Author', 'Category', 'Date', 'Count'])



    def get_author_hashtags(self, df):
        df_temp = df.set_index('Author')
        x = df_temp['Contents'].apply(lambda x : extract_hash_tags(x))
        hashtags=collections.defaultdict(lambda: collections.defaultdict(lambda : 1))
        for author, frame in x.iteritems():
            for ht in frame:
                ht = normalize_hashtag(ht)
                if ht in hashtags.get(author, {}):
                    hashtags[author][ht] = hashtags[author][ht] + 1
                else:
                    hashtags[author][ht] = 1
        l = []
        for key1, value1 in hashtags.iteritems():
            for key2, value2 in sorted(value1.iteritems(), key=lambda(k,v): (v,k), reverse=True):
                d = {}
                d.update({'Author':key1, 'Hashtag':key2, 'Count':value2})
                l.append(d)
        return pd.DataFrame(l, columns=['Author', 'Hashtag', 'Count'])

    # Must be passed a DataFrame with a Domain column
    def get_author_hosts(self, df):
        return pd.crosstab(df['Author'], df['Domain'])
        '''
        x = df_temp['Contents'].apply(lambda x : extract_hosts(x))

        hosts = collections.defaultdict(lambda: collections.defaultdict(lambda : 1))
        for author, host in x.iteritems():
            if host in hosts.get(author, {}):
                hosts[author][host] += hosts[author][host]
            else:
                hosts[author][host] = 1
        l = []
        for key1, value1 in hosts.iteritems():
            d = {}
            for key2, value2 in sorted(value1.iteritems(), key=lambda(k,v): (v,k), reverse=True):
                d.update({'author':key1, 'host':key2, 'count':value2})
            l.append(d)
        print l
        return pd.DataFrame(l)
        '''

    def get_author_category_hosts(self, df):
        return pd.crosstab([df['Author'], df['Category']], df['Domain'])


    def get_author_mentions(self, df):
        #df_orig_mentions = df.apply(lambda row : extract_mentions(row[u'Contents'], row[u'Author'], axis=1))
        df_Auth_Mentions = pd.DataFrame()
        p = re.compile(ur'@(\w{1,15})\b')
        for index, row in df.iterrows():
            mentions = re.findall(p, row['Contents'])
            dict1 = {'Author':row['Author'], 'Mention':mentions}
            df_temp = pd.DataFrame(dict1)
            if len(mentions) > 0:
                df_Auth_Mentions = pd.concat([df_Auth_Mentions, df_temp])
        return pd.crosstab(df_Auth_Mentions['Author'], df_Auth_Mentions['Mention'])

    def get_author_category_mentions(self, df):
        df_Auth_Mentions = pd.DataFrame()
        p = re.compile(ur'@(\w{1,15})\b')
        for index, row in df.iterrows():
            mentions = re.findall(p, row['Contents'])
            dict1 = {'Author':row['Author'], 'Category':row['Category'], 'Mention':mentions}
            df_temp = pd.DataFrame(dict1)
            if len(mentions) > 0:
                df_Auth_Mentions = pd.concat([df_Auth_Mentions, df_temp])
        return pd.crosstab([df_Auth_Mentions['Author'], df_Auth_Mentions['Category']], df_Auth_Mentions['Mention'])

    def get_author_retweets(self, df):
        pattern = re.compile(ur'RT @(\w{1,15})\b')
        df['original_author'] = df['Contents'].apply(lambda x : extract_original_tweeter(x, pattern))
        df_retweets = df.query("original_author != ''")
        l = []
        for ind, row in df_retweets.iterrows():
            d = {'Author':row['Author'], 'RT':row['original_author']}
            l.append(d)
        df_output = pd.DataFrame(l)
        return pd.crosstab(df_output['Author'], df_output['RT'])

    def get_top_auth_rts(self, df, amount):
        l_auths = []
        l_cats = []
        for ind, row in df.iterrows():
            a = row.transpose()
            a = a.order(ascending=False)
            auth = {'Author':ind[0], 'RTs':a[0:amount]}
            cat = {'Author':ind[0], 'Category':ind[1], 'RTs':a[0:amount]}
            l_auths.append(auth)
            l_cats.append(cat)
        return [l_auths, l_cats]

    def get_top_auth_mentions(self, df, amount):
        l_auths = []
        l_cats = []
        for ind, row in df.iterrows():
            a = row.transpose()
            a = a.order(ascending=False)
            auth = {'Author':ind[0], 'Mentions':a[0:amount]}
            cat = {'Author':ind[0], 'Category':ind[1], 'Mentions':a[0:amount]}
            l_auths.append(auth)
            l_cats.append(cat)
        return [l_auths, l_cats]




    def get_author_rt_stats(self, df):
        sm = df['Count'].groupby(df['Author']).sum()
        cnt = df['Count'].groupby(df['Author']).count()
        rt_min = df['Count'].groupby(df['Author']).min()
        rt_max = df['Count'].groupby(df['Author']).max()
        rt_mean = df['Count'].groupby(df['Author']).mean()
        l = []
        for index, value in cnt.iteritems():
            d = {}
            d.update({'Author':index, 'Count':value, 'Sum':sm[index], 'Min':rt_min[index], 'Max':rt_max[index], 'Mean':rt_mean[index]})
            l.append(d)
        return pd.DataFrame(l, columns=['Author', 'Count', 'Sum', 'Mean', 'Min', 'Max'])

    def get_author_category_retweet_stats(self, df, df_raw):
        auths = self.get_author_counts(df_raw)
        cats = self.get_category_counts(df_raw)
        sm = df['count'].groupby([df['category'], df['author']]).sum()
        cnt = df['count'].groupby([df['category'], df['author']]).count()
        l = []
        for cat_ind, cat_val in cats.iteritems():
            for auth_ind, auth_val in auths.iteritems():
                d = {}
                try:
                    d.update({'category':cat_ind, 'author':auth_ind, 'count':cnt[cat_ind][auth_ind], 'sum': sm[cat_ind][auth_ind]})
                except:
                    d.update({'category':cat_ind, 'author':auth_ind, 'count':0, 'sum': 0})
                finally:
                    l.append(d)
        return pd.DataFrame(l, columns=['Author', 'Category', 'Count', 'Sum'])


    # Saves a day by day tracking of retweets
    # Takes in two DataFrames:
    def trace_tweets(self, df_raw_orig, df_raw_retweets, max_days, min_retweets):
        retweets = self.get_retweet_list(df_raw_retweets)
        keys = ['Author', 'Category', 'Orig_date', 'Tweet', 'Count']
        p2 = re.compile(ur'(RT @\w{1,15})\b[: | |:](.*)$')
        tweet_contents = ""
        rt_timelines = []
        for index, value in retweets.iteritems():

            if value > min_retweets:
                keys = ['Author', 'Category', 'Orig_date', 'Tweet', 'Count']
                dict_rt_timeline = dict.fromkeys(keys)
                rt_text = re.split(p2, index)
                for x in rt_text:
                    if len(x) > 0 and not x.startswith('RT @'):
                        tweet_contents = x.strip(' :"''')

                # Ensure that there actually was a tweet found
                if len(tweet_contents) > 0:
                    orig_result = df_raw_orig[df_raw_orig['Contents'].str.contains(re.escape(tweet_contents))]
                    # Ensure it matches up to something in the first set of tweets
                    if len(orig_result) > 0:
                        dict_rt_timeline['Tweet'] = orig_result.iloc[0]['Contents']
                        dict_rt_timeline['Author'] = orig_result.iloc[0]['Author']
                        dict_rt_timeline['Category'] = orig_result.iloc[0]['Category']
                        dict_rt_timeline['Orig_date'] = dt.datetime.strftime(dt.datetime.strptime(str(orig_result.iloc[0]['Date (GMT)']), '%Y-%m-%d %X'), '%Y-%m-%d')
                        dict_rt_timeline['Count'] = value

                        # Now go back and search on list of retweets
                        rt_results = df_raw_retweets[df_raw_retweets['Contents'].str.contains(re.escape(tweet_contents))]
                        rt_dates = []

                        # Get a list of counts by day
                        for index, row in rt_results.iterrows():
                            rt_dates.append(dt.datetime.strftime(dt.datetime.strptime(str(row['Date']), '%Y-%m-%d %X'), '%Y-%m-%d'))
                        date_count = collections.Counter(rt_dates)

                        # Now sort them by days since original tweet
                        # Search on start day to max_days
                        # Future: allow this to be done on an hourly basis
                        dt_first_tweet = dt.datetime.strptime(dict_rt_timeline['Orig_date'], '%Y-%m-%d')
                        count_by_days = {}
                        for i in range(0, max_days):
                            new_date = dt_first_tweet + dt.timedelta(days=i)
                            curr_key = 'Day' + str('%02d' % i)
                            curr_val = date_count[dt.datetime.strftime(new_date, '%Y-%m-%d')]
                            keys.append(curr_key)
                            count_by_days.update({curr_key:curr_val})
                        # Save to dictionary
                        #dict_rt_timeline['rt_dates'] = count_by_days
                        x = dict(count_by_days.items()+dict_rt_timeline.items())
                        rt_timelines.append(x)
        return pd.DataFrame(rt_timelines, columns=keys)
        #return pd.DataFrame(rt_timelines)



    def get_rt_locations(self, df_raw_rts, df_raw_orig):
        retweets = self.get_retweet_list(df)
        keys = ['Author', 'Category', 'Locations']

    # Category related functions. Each gives data aggregated per author
    # - Overall Count
    # - Daily Count
    # - Hashtags Count
    # - Hosts Count
    # - Retweet Counts
    def get_category_counts(self, df):
        return df['Category'].value_counts()

    def get_category_daily_counts(self, df):
        resamp = df.set_index('Date').groupby('Category').resample('D', how='count')
        l = []
        for ind, frame in resamp.iterrows():
            d = {}
            d.update({'Category': ind[0], 'Date':dt.datetime.strftime(ind[1], '%Y-%m-%d'), 'Count':frame[0]})
            l.append(d)
        return pd.DataFrame(l, columns=['Category', 'Date', 'Count'])

    def get_category_hashtags(self, df):
        df_temp = df.set_index('Category')
        x = df_temp['Contents'].apply(lambda x : extract_hash_tags(x))
        hashtags=collections.defaultdict(lambda: collections.defaultdict(lambda : 1))
        for author, frame in x.iteritems():
            for ht in frame:
                ht = normalize_hashtag(ht)
                if ht in hashtags.get(author, {}):
                    hashtags[author][ht] = hashtags[author][ht] + 1
                else:
                    hashtags[author][ht] = 1
        l = []
        for key1, value1 in hashtags.iteritems():
            for key2, value2 in sorted(value1.iteritems(), key=lambda(k,v): (v,k), reverse=True):
                d = {}
                d.update({'Category':key1, 'Hashtag':key2, 'Count':value2})
                l.append(d)
        return pd.DataFrame(l, columns=['Category', 'Hashtag', 'Count'])

    def get_category_hosts(self, df):
        return pd.crosstab(df['Category'], df['Domain'])

        '''
        df_temp = df.set_index('Category')
        x = df_temp['Contents'].apply(lambda x : extract_hosts(x))

        hosts = collections.defaultdict(lambda: collections.defaultdict(lambda : 1))
        for author, host in x.iteritems():
            if host in hosts.get(author, {}):
                hosts[author][host] += hosts[author][host]
            else:
                hosts[author][host] = 1
        l = []
        for key1, value1 in hosts.iteritems():
            d = {}
            for key2, value2 in sorted(value1.iteritems(), key=lambda(k,v): (v,k), reverse=True):
                d.update({'author':key1, 'host':key2, 'count':value2})
            l.append(d)
        print l
        return pd.DataFrame(l)
        '''

    def get_category_rt_counts(self, df):
        sm = df['Count'].groupby(df['Category']).sum()
        cnt = df['Count'].groupby(df['Category']).count()
        l = []
        for index, value in cnt.iteritems():
            d = {}
            d.update({'Category':index, 'Count':value, 'Sum':sm[index]})
            l.append(d)
        return pd.DataFrame(l)




