CH Toolbox 0.1
==============
Author: Galen Stocking, gstocking@gmail.com
This module provides a set of tools for extracting descriptive statistics from a Crimson Hexagon Twitter dataset. A dataset is a directory of Bulk Exported Crimson Hexagon files. All functions return .csv files. Future versions will include limited visualization functionality.

Basic summary information:
- List of domain names tweeted, ranked by count (descending)
- List of hashtags tweeted, ranked by count (descending)
- List of each Twitter user and the other users they mention (edgelist for Gephi)
- List of retweets, ranked by count (descending)

It currently can also summarize some statistics according to author and category.
Some of these functions require a threshold for inclusion (ie. to exclude authors with just one tweet).

* Categories *
- List of categories, ranked by number of tweets (descending)
- Daily count of tweets per category
- List of hashtags used per category, ranked by count (descending)
- List of each Twitter user and the other users they mention (edgelist for Gephi)
- List of domain names tweeted per category, ranked by count (descending)

* Authors *
- List of authors, ranked by number of tweets (descending)
- Daily count of tweets per author
- List of hashtags used per author, ranked by count (descending)
- List of each Twitter user and the other users they mention (edgelist for Gephi)
- List of domain names tweeted per author, ranked by count (descending)
- Count of tweets by author / category

There are also statistics to show the lifecycle of a tweet in the network from a subset of authors, eg. how long an influential user's tweets are retweeted and the shape of that curve. This requires two datasets: one of the originating authors, another of all retweets. 
- List of retweeted Tweets with daily counts of retweets. Requires a maximum number of days for analysis; default is 20.
-- Future versions will allow:
--- single dataset with a list of original authors
--- single dataset without a list (e.g. finds all retweets in dataset)
--- more fine grained aggregation (e.g. by the hour, 15 minute segments, etc.)
- List of author / category pairs and count of retweets