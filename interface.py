__author__ = 'galenstocking'

from CH_Toolbox import CH_Toolbox
import pandas as pd
import argparse
import unicodecsv as csv
import os

fn_retweets = "retweets.csv"
fn_hashtags = "hashtags.csv"
fn_hosts = "hosts.csv"
fn_timelines = "timelines.csv"
fn_cat_counts = "catcount.csv"
fn_cat_daily_counts = "catdailycount.csv"
fn_cat_hashtags = "cathashtags.csv"
fn_cat_hosts = "cathosts.csv"
fn_cat_retweets = "catretweets.csv"
fn_auth_counts = "authcount.csv"
fn_auth_daily_counts  = "authdailycount.csv"
fn_auth_cat_daily_counts = "authcatdailycount.csv"
fn_auth_hashtags = "authhashtags.csv"
fn_auth_hosts = "authhosts.csv"
fn_auth_mentions = "authmentions.csv"
fn_auth_cat_mentions = "authcatmentions.csv"
fn_auth_retweets = "authretweets.csv"
fn_mentions = "mentions.csv"

fn_auth_rt_stats = 'authrtstats.csv'
fn_cat_rt_stats = 'catrtstats.csv'
fn_auth_cat_rt_stats = 'authcatrtstats.csv'

fn_cat_rttimeline = "catrttimeline.csv"
fn_auth_rttimeline = "authrttimeline.csv"

fn_auth_cat_hosts = "authcathosts.csv"

def makefilesafe(fn):
    for char in fn:
        if char in "?!/;:":
            fn = fn.replace(char,'')
    return fn


if __name__ == "__main__":

    cht = CH_Toolbox()
    tweet_threshold = 50
    max_days = 20
    top_count = 25

    parser = argparse.ArgumentParser(prog='CH_Toolbox')
    parser.add_argument('output_directory', help="Directory to save resulting csv files")
    parser.add_argument('retweet_directory', help="Directory of retweets. Must contain CH .xls files")
    parser.add_argument('author_directory', help="Directory of original author tweets. Must contain CH .xls files")
    parser.add_argument('-mt', '--min_tweets', help="Minimum number of tweets to be included in tweet trace. Default = 50", type=int)
    parser.add_argument('-days', '--max_days', help="Maximum number of days to analyze in tweet trace. Default = 20", type=int)
    parser.add_argument('-top', '--top_count', help="Amount to show in top mentions and RTs", type=int)

    args = parser.parse_args()

    # Process User Args
    rt_dirname = args.retweet_directory
    out_dirname = args.output_directory
    auth_dirname = args.author_directory

    if args.min_tweets:
        tweet_threshold = int(args.min_tweets)
    if args.max_days:
        max_days = args.max_days
    if args.top_count:
        top_count = args.top_count

    # Load files
    print "Loading files..."
    df_rts = cht.load_files(rt_dirname)
    df_auths = cht.load_files(auth_dirname)


    # When that is done, run general statistics over the combined dataset
    print "*** Basic summary statistics ***"
    print "Saving retweets..."
    retweets = cht.get_retweet_list(df_rts)
    retweets.to_csv(os.path.join(out_dirname, fn_retweets), encoding='utf-8', header=['Count'], index_label='Retweet')

    print "Saving hashtags..."
    hashtags = cht.get_hashtag_list(df_rts)
    with open(os.path.join(out_dirname, fn_hashtags), 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['Hashtag', 'Count'])
        writer.writerows(hashtags)


    print "*** Category summary statistics ***"

    print "Saving category count.."
    cat_counts = cht.get_category_counts(df_auths)
    cat_counts.to_csv(os.path.join(out_dirname, fn_cat_counts), header=['Count'], index_label='Category')
    '''
    with open(os.path.join(out_dirname, fn_cat_counts), 'w') as f:
        writer = csv.writer(f)
        writer.writerows(cat_counts)
    '''
    print "Saving category daily count.."
    cat_daily_counts = cht.get_category_daily_counts(df_auths)
    cat_daily_counts.to_csv(os.path.join(out_dirname, fn_cat_daily_counts))

    print "Saving category hashtags..."
    cat_hashtags = cht.get_category_hashtags(df_auths)
    cat_hashtags.to_csv(os.path.join(out_dirname, fn_cat_hashtags))

    #print "Saving category retweets.."
    # Determine who the original groups are mentioning the most
    #cat_retweets = cht.get_category_rt_counts(df_auths)
    #cat_retweets.to_csv(os.path.join(out_dirname, fn_cat_retweets), encoding='utf-8')

    print "*** Author summary statistics ***"

    print "Saving author count.."
    auth_counts = cht.get_author_counts(df_auths)
    auth_counts.to_csv(os.path.join(out_dirname, fn_auth_counts), header=['Count'], index_label='Author')
    '''
    with open(os.path.join(out_dirname, fn_auth_counts), 'w') as f:
        writer = csv.writer(f)
        writer.writerows(auth_counts)
    '''

    print "Saving author daily count.."
    auth_daily_counts = cht.get_author_daily_counts(df_auths)
    auth_daily_counts.to_csv(os.path.join(out_dirname, fn_auth_daily_counts))

    print "Saving author / category daily count.."
    auth_cat_daily_counts = cht.get_author_category_daily_counts(df_auths)
    auth_cat_daily_counts.to_csv(os.path.join(out_dirname, fn_auth_cat_daily_counts))

    print "Saving author hashtags..."
    auth_hashtags = cht.get_author_hashtags(df_auths)
    auth_hashtags.to_csv(os.path.join(out_dirname, fn_auth_hashtags))

    print "Saving author mentions..."
    # Determine who the original groups are mentioning the most
    ct_mentions = cht.get_author_mentions(df_auths)
    ct_mentions.to_csv(os.path.join(out_dirname, fn_auth_mentions), encoding='utf-8')
    ct_auth_cat_mentions = cht.get_author_category_mentions(df_auths)
    ct_auth_cat_mentions.to_csv(os.path.join(out_dirname, fn_auth_cat_mentions))
    all_top_mentions = cht.get_top_auth_mentions(ct_auth_cat_mentions, top_count)
    auth_top_mentions = all_top_mentions[0]
    auth_cat_top_mentions = all_top_mentions[1]
    for top_mentions in auth_top_mentions:
        fn = makefilesafe('topmentions_' + top_mentions['Author'] + '.csv')
        mentions = top_mentions['Mentions']
        mentions.to_csv(os.path.join(out_dirname, fn), encoding='utf-8')
    for top_mentions in auth_cat_top_mentions:
        fn = makefilesafe('topmentions_' + top_mentions['Author'] + '_' + top_mentions['Category'] + '.csv')
        mentions = top_mentions['Mentions']
        mentions.to_csv(os.path.join(out_dirname, fn), encoding='utf-8')


    print "Saving author retweets..."
    # Determine who the original groups are mentioning the most
    ct_auth_retweets = cht.get_author_retweets(df_auths)
    ct_auth_retweets.to_csv(os.path.join(out_dirname, fn_auth_retweets), encoding='utf-8')
    all_top_RTs = cht.get_top_auth_rts(ct_auth_retweets, top_count)
    auth_top_RTs = all_top_RTs[0]
    auth_cat_top_RTs = all_top_RTs[1]
    for top_RTs in auth_top_RTs:
        fn = makefilesafe('topRTs_' + top_RTs['Author'] + '.csv')
        RTs = top_RTs['RTs']
        RTs.to_csv(os.path.join(out_dirname, fn), encoding='utf-8')
    for top_RTs in auth_cat_top_RTs:
        fn = makefilesafe('topRTs_' + top_RTs['Author'] + '_' + top_RTs['Category'] + '.csv')
        RTs = top_RTs['RTs']
        RTs.to_csv(os.path.join(out_dirname, fn), encoding='utf-8')


    print "Analyzing retweets..."
    df_rt_timelines = cht.trace_tweets(df_auths, df_rts, max_days, tweet_threshold)

    df_rt_cat_count = cht.get_category_rt_counts(df_rt_timelines)
    df_rt_auth_count = cht.get_author_rt_stats(df_rt_timelines)
    #df_rt_auth_cat_count = cht.get_author_category_retweet_stats(df_rt_timelines, df_auths)

    #df_Locations = cht.get_rt_locations(df_rt_timelines)

    print "Saving retweet information.."
    df_rt_timelines.to_csv(os.path.join(out_dirname, fn_timelines), encoding='utf-8')
    df_rt_cat_count.to_csv(os.path.join(out_dirname, fn_cat_rt_stats), encoding='utf-8')
    df_rt_auth_count.to_csv(os.path.join(out_dirname, fn_auth_rt_stats), encoding='utf-8')
    #df_rt_auth_cat_count.to_csv(os.path.join(out_dirname, fn_auth_cat_rt_stats), encoding='utf-8')


    print "*** Hosts ***"

    print "Finding overall hosts.."

    df_hosts = cht.get_hosts_list(df_auths)
    #df_hosts = cht.get_hosts_list(df_auths[:100])
    hosts_count = cht.get_hosts_count(df_hosts)
    hosts_count.to_csv(os.path.join(out_dirname, fn_hosts), encoding='utf-8')
    '''
    with open(os.path.join(out_dirname, fn_hosts), 'w') as  f:
        writer = csv.writer(f)
        writer.writerows(hosts_count)
    '''

    print "Finding author hosts.."
    auth_hosts = cht.get_author_hosts(df_hosts)
    auth_hosts.to_csv(os.path.join(out_dirname, fn_auth_hosts), encoding='utf-8')

    print "Finding category hosts.."
    cat_hosts = cht.get_category_hosts(df_hosts)
    cat_hosts.to_csv(os.path.join(out_dirname, fn_cat_hosts), encoding='utf-8')

    print "Finding author / category joint hosts..."
    auth_cat_hosts = cht.get_author_category_hosts(df_hosts)
    auth_cat_hosts.to_csv(os.path.join(out_dirname, fn_auth_cat_hosts), encoding='utf-8')